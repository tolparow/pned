import os
import shutil

from os import listdir
from os.path import isfile, join

MAIN_DIR = '/Users/tolparow/Downloads/all'
DATASET_INFO_CSV_FILE = os.path.join(MAIN_DIR, 'stage_1_train_labels.csv')
LABELED_SAMPLES_DIR = os.path.join(MAIN_DIR, 'stage_1_train_images')

onlyfiles = [f for f in listdir(LABELED_SAMPLES_DIR) if isfile(join(LABELED_SAMPLES_DIR, f))]

i = 0
i_max = [3500, 7000, 11000, 14500, 18000, 21500, 3000]
for file in onlyfiles:
    k = 0
    for j in range(len(i_max)):
        if i < i_max[j]:
            k = j
            break

    i += 1

    shutil.copyfile(os.path.join(LABELED_SAMPLES_DIR, file), os.path.join(MAIN_DIR, str(k), file))
