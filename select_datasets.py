import os
import shutil

import pandas as pd
import numpy as np
from shutil import copyfile

MAIN_DIR = '/Users/tolparow/Downloads/all'
DATASET_INFO_CSV_FILE = os.path.join(MAIN_DIR, 'stage_1_train_labels.csv')
LABELED_SAMPLES_DIR = os.path.join(MAIN_DIR, 'stage_1_train_images_png')

TRAIN_DIR = 'train'
VALIDATE_DIR = 'validate'

TRAIN_POS_DIR = os.path.join(TRAIN_DIR, 'positive')
TRAIN_NEG_DIR = os.path.join(TRAIN_DIR, 'negative')

VALIDATE_POS_DIR = os.path.join(VALIDATE_DIR, 'positive')
VALIDATE_NEG_DIR = os.path.join(VALIDATE_DIR, 'negative')

N_TRAIN = int(1024 * 1.005)
N_VALIDATE = int(128 * 1.005)


def recreate_dir(dir_path):
    if os.path.exists(dir_path) and os.path.isdir(dir_path):
        shutil.rmtree(dir_path)
    os.makedirs(dir_path)


def copy_samples(_samples, pos_dir, neg_dir):
    for sample_key in _samples.keys():
        sample_file_name = sample_key + '.dcm.png'
        copyfile(os.path.join(LABELED_SAMPLES_DIR, sample_file_name),
                 os.path.join(pos_dir if int(_samples[sample_key]) else neg_dir, sample_file_name))


if __name__ == '__main__':
    samples = pd.read_csv(DATASET_INFO_CSV_FILE)

    train_samples = samples.sample(N_TRAIN)
    validate_samples = samples.sample(N_VALIDATE)

    train_samples = {s['patientId']: s['Target'] for s in train_samples.to_dict(orient='records')}
    validate_samples = {s['patientId']: s['Target'] for s in validate_samples.to_dict(orient='records')}

    recreate_dir(TRAIN_POS_DIR)
    recreate_dir(TRAIN_NEG_DIR)
    recreate_dir(VALIDATE_POS_DIR)
    recreate_dir(VALIDATE_NEG_DIR)

    copy_samples(train_samples, TRAIN_POS_DIR, TRAIN_NEG_DIR)
    copy_samples(validate_samples, VALIDATE_POS_DIR, VALIDATE_NEG_DIR)
