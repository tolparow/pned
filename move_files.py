import os
import shutil
from os.path import isfile, join

MAIN_DIR = '/Users/tolparow/Downloads/all'
DATASET_INFO_CSV_FILE = os.path.join(MAIN_DIR, 'stage_1_train_labels.csv')
LABELED_SAMPLES_DIR = os.path.join(MAIN_DIR, 'stage_1_train_images_png')

for k in range(0, 6):
    print(k)
    onlyfiles = [f for f in os.listdir(LABELED_SAMPLES_DIR + str(k)) if isfile(join(LABELED_SAMPLES_DIR + str(k), f))]

    for file in onlyfiles:
        shutil.move(join(LABELED_SAMPLES_DIR + str(k), file), os.path.join(LABELED_SAMPLES_DIR, file))
