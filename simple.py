import os

from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator

os.environ['KMP_DUPLICATE_LIB_OK']='True'

TRAINING_DIR = 'train'
VALIDATION_DIR = 'validate'

input_shape = (224, 224, 3)

classifier = Sequential()

classifier.add(Conv2D(64, (3, 3), input_shape=input_shape, activation='relu'))

classifier.add(MaxPooling2D(pool_size=(2, 2)))

classifier.add(Flatten())

classifier.add(Dense(1024, activation='relu'))

classifier.add(Dense(2, activation='sigmoid'))

classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

training_generator = ImageDataGenerator(
    rescale=1. / 224,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

validation_generator = ImageDataGenerator(rescale=1. / 224)

training_data_generator = training_generator.flow_from_directory(directory=TRAINING_DIR,
                                                                 target_size=(224, 224),
                                                                 batch_size=64,
                                                                 class_mode='categorical',
                                                                 shuffle=True)

validation_data_generator = validation_generator.flow_from_directory(directory=VALIDATION_DIR,
                                                                     target_size=(224, 224),
                                                                     batch_size=32,
                                                                     class_mode='categorical',
                                                                     shuffle=True)

classifier.fit_generator(generator=training_data_generator,
                         epochs=8,
                         verbose=2,
                         validation_data=validation_data_generator,
                         steps_per_epoch=128,
                         validation_steps=16
                         )
